function initFollow() {
    let app = new Vue({
        el: '#follow',
        data: {
            username: '',
            queue: [],
            running: false
        },
        mounted() {
            const socket = io('http://192.168.1.80:3000');
            socket.on('follow', (data) => {
                this.queue.push(data.name);
                if (!this.running) this.startShowingFollows()
            });
        },
        methods: {
            startShowingFollows() {
                this.running = true;
                this.username = '';
                if (this.queue.length) {
                    this.username = this.queue.shift()
                    setTimeout(this.startShowingFollows, 5000)
                } else {
                    this.running = false
                }
            }
        }
    })
}

initFollow()